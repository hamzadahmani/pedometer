/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import { ApplicationProvider, IconRegistry} from '@ui-kitten/components';
import { EvaIconsPack } from '@ui-kitten/eva-icons';
import * as eva from '@eva-design/eva';
import Home from './src/components/home';
import History from './src/components/history';
import store from './src/store/store';
import { Provider } from "react-redux";
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { StatusBar } from 'react-native';

const App: () => React$Node = () => {
 
const Tab = createBottomTabNavigator();
  return (
    <>
      <IconRegistry icons={EvaIconsPack} />
      <ApplicationProvider {...eva} theme={eva.light}>
      <StatusBar
                     barStyle="light-content"
                    translucent={true}
                    backgroundColor="black"
                />
      <Provider store={store}>
       <NavigationContainer>
      <Tab.Navigator
         screenOptions={({ route }) => ({
          tabBarIcon: ({ focused, color, size }) => {
            let iconName;

            if (route.name === 'Accueil') {
              iconName = focused
                ? 'home'
                : 'home-outline';
            } else if (route.name === 'Historique') {
              iconName = focused ? 'stopwatch' : 'stopwatch-outline';
            }

            // You can return any component that you like here!
            return <Ionicons name={iconName} size={size} color={color} />;
          },
        })}
        tabBarOptions={{
          activeTintColor: 'tomato',
          inactiveTintColor: 'gray',
        }}
        >
        <Tab.Screen name="Accueil" component={Home} />
        <Tab.Screen name="Historique" component={History} />
      </Tab.Navigator>
      </NavigationContainer>
      </Provider>
      </ApplicationProvider>
    </>
  );
};

 
export default App;
