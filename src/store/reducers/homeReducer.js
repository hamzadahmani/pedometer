import { WEEKLY_STEPS, DAILY_STEPS ,VALID_STEPS} from '../actions/type';

/***
 * Initiale State
 */
const initialState = {
    Daily: { value:0 },
    Weekly: [],
    ValidSteps:{ date:"1590-01-01",jackpot:0, valide: false ,StepsValid:0}
}

export default (state = initialState, action) => {
    const { type, payload } = action;

    switch (type) {
        case WEEKLY_STEPS:
            return {
                ...state,
                Weekly: payload,
            };
        case DAILY_STEPS:
            return {
                ...state,
                Daily: payload,
            }
        case VALID_STEPS:
            return {
                ...state,
                ValidSteps: payload,
            }
            
        default:
            return state
    }
}
