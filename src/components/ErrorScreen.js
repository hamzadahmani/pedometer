import React from 'react';
import {Layout, Text} from '@ui-kitten/components';
import {StyleSheet, TouchableHighlight, View} from 'react-native';
 import BugFixing from '../assets/BugFixing.svg';
  export const ErrorScreen = (props) => {
 
  return (
    <Layout style={[styles.container && styles.radius_40]}>
      <BugFixing
        width={250}
        height={250}
      />
      <Text category="s2" appearance="hint">
        Error screen
      </Text>
      <View style={styles.mt_20}>
        <TouchableHighlight
          activeOpacity={0.6}
          underlayColor="#DDDDDD"
          onPress={props.refreshComponent}>
          <Text
            style={styles.refresh_btn}
            status="info"
            category="s2"
            appearance="hint">
            Refresh
          </Text>
        </TouchableHighlight>
      </View>
    </Layout>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  refresh_btn: {
    textDecorationLine: 'underline',
  },
  radius_40: {
    borderRadius: 40,
  },
  mt_20: {
    marginTop: 20,
  },
});
