import React, { useEffect, useState } from 'react';
import {
  StyleSheet,
  View,
} from 'react-native';
import {
  Text,
} from '@ui-kitten/components';
import Icon from 'react-native-vector-icons/FontAwesome5';
import StepIndicator from 'react-native-step-indicator';

const StepsBar = (props) => {
  const [currentPosition, setCurrentPosition] = useState(0)
  const labels = ["1500", "3000", "6500", "10000", "15000", "20000"];
  const customStyles = {
    stepIndicatorSize: 30,
    currentStepIndicatorSize: 30,
    separatorStrokeWidth: 2,
    currentStepStrokeWidth: 3,
    stepStrokeCurrentColor: '#fe7013',
    stepStrokeWidth: 3,
    stepStrokeFinishedColor: '#fe7013',
    stepStrokeUnFinishedColor: '#aaaaaa',
    separatorFinishedColor: '#fe7013',
    separatorUnFinishedColor: '#aaaaaa',
    stepIndicatorFinishedColor: '#fe7013',
    stepIndicatorUnFinishedColor: '#ffffff',
    stepIndicatorCurrentColor: '#ffffff',
    stepIndicatorLabelFontSize: 10,
    currentStepIndicatorLabelFontSize: 13,
    stepIndicatorLabelCurrentColor: '#fe7013',
    stepIndicatorLabelFinishedColor: '#ffffff',
    stepIndicatorLabelUnFinishedColor: '#aaaaaa',
    labelColor: '#ffffff',
    labelSize: 13,
    currentStepLabelColor: '#fe7013'
  }
  const renderStepIndicator = (params) => {
    const array = ['1$', '3$', '6$', '10$', '18$', '25$']
    return <Text style={{ fontSize: 10 }}>{array[params.position]}</Text>
  };

  useEffect(() => {
  //set stepsBar Indicator with courant progress 
    switch (true) {
      case (props.wallking >= 1500 && props.wallking < 3000):
        setCurrentPosition(0)
        break;   //<== exit from the swtich, return into useEffect
      case (props.wallking >= 3000 && props.wallking < 6500):
        setCurrentPosition(1)
        break;   //<== exit from the swtich, return into useEffect
      case (props.wallking >= 6500 && props.wallking < 10000):
        setCurrentPosition(2)
        break;   //<== exit from the swtich, return into useEffect
      case (props.wallking >= 10000 && props.wallking < 15000):
        setCurrentPosition(3)
        break;   //<== exit from the swtich, return into useEffect
      case (props.wallking >= 15000 && props.wallking < 20000):
        setCurrentPosition(4)
        break;   //<== exit from the swtich, return into useEffect 
      case (props.wallking >= 20000):
        setCurrentPosition(5)
        break;   //<== exit from the swtich, return into useEffect
      default:
        setCurrentPosition(null)
      //automatic    exit from the swtich, return into useEffect
    }
  }, [props.wallking])
  return (
    <>


      <View style={styles.container}>
        <View style={{ flex: 1, justifyContent: 'center', flexDirection: 'column' }}>
          <StepIndicator
            customStyles={customStyles}
            currentPosition={currentPosition}
            labels={labels}
            direction={'vertical'}
            stepCount={6}
            renderStepIndicator={renderStepIndicator}
          />
          <Icon style={styles.icon} name="medal" size={30} color="#FFD700" />
        </View>
      </View>

    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  icon:{ marginTop: -30, marginBottom: 20 }

});

export default StepsBar;
