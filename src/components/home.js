import React, { useEffect, useState ,useCallback} from 'react';
import {
    StyleSheet,
    ImageBackground,
    View,
 } from 'react-native';
import { useSelector ,useDispatch } from 'react-redux'
import StepsBar from './stepsBar';
import { Text, Button } from '@ui-kitten/components';
import Icon from 'react-native-vector-icons/FontAwesome5';
import ModalWithBackdropShowcase from "./modal";
import { runPermission,setData } from '../store/actions/homeAction';
import ErrorBoundary from '../errorBoundary/ErrorBoundary';
import moment from 'moment'

const Home = () => {
     const [openModal, setOpenModal] = useState({message:'',open:false})
    const disaptch=useDispatch()
    const steps = useSelector(state => state.homeReducer.Daily)
    const ValidSteps = useSelector(state => state.homeReducer.ValidSteps)
    const [, updateState] = useState();
    const forceUpdate = useCallback(() => updateState({}), []);
  
    useEffect(() => {
        disaptch(runPermission())
    }, [])

  

    const refreshComponent = () => {
        forceUpdate();
      };

    const handelClick = async () => {
        console.log('ValidSteps',ValidSteps);
        if(moment(ValidSteps.date).format('YYYY-MM-DD') !=moment(new Date).format('YYYY-MM-DD')){
        switch (true) {
            case (steps.value >= 1500 && steps.value < 3000):
                    disaptch(setData( {
                    date: new Date(),
                    jackpot: ValidSteps.jackpot + 1,
                    StepsValid: steps.value,
                    valide: true
                }))
                break;   //<== exit from the swtich, return into handelClick
            case (steps.value >= 3000 && steps.value < 6500):
                disaptch(setData( {
                date: new Date(),
                jackpot: ValidSteps.jackpot + 3,
                StepsValid: steps.value,
                valide: true
            }))
                break;   //<== exit from the swtich, return into handelClick
            case (steps.value >= 6500 && steps.value < 10000):
                    disaptch(setData( {
                    date: new Date(),
                    jackpot: ValidSteps.jackpot + 6,
                    StepsValid: steps.value,
                    valide: true
                }))
                break;   //<== exit from the swtich, return into handelClick
            case (steps.value >= 10000 && steps.value < 15000):
                    disaptch(setData( {
                    date: new Date(),
                    jackpot: ValidSteps.jackpot + 10,
                    StepsValid: steps.value,
                    valide: true
                }))
                break;   //<== exit from the swtich, return into handelClick
            case (steps.value >= 15000 && steps.value < 20000):
                    disaptch(setData( {
                    date: new Date(),
                    jackpot: ValidSteps.jackpot + 15,
                    StepsValid: steps.value,
                    valide: true
                }))
                break;   //<== exit from the swtich, return into handelClick 
            case (steps.value >= 20000):
                    disaptch(setData( {
                    date: new Date(),
                    jackpot: ValidSteps.jackpot + 25,
                    StepsValid: steps.value,
                    valide: true
                }))
                break;   //<== exit from the swtich, return into handelClick
            default:
                setOpenModal({message:`Vous avez besoin plus de ${1500 - steps.value} pas pour gagner votre première récompense` ,open:true})
             //automatic    exit from the swtich, return into handelClick
        }
    }else{
        setOpenModal({message:`Vous deja validé ${ValidSteps.StepsValid} pas` ,open:true})
    }
    }
    return (
        <ErrorBoundary refreshComponent={refreshComponent}>
            <ImageBackground source={require('../assets/backgrond.jpg')} style={styles.image}>
                <View style={{ flexDirection: 'row' }}>
                    <Icon name="wallet" size={30} color="#100" />
                    <Text style={{ color: '#100', marginLeft: 5 }} category='h5'>{ValidSteps.jackpot + '$'}</Text>
                </View>
                <View style={{ flex: 1, flexDirection: 'row' }}>
                    <StepsBar wallking={steps.value} />
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'space-around' }}>
                        <View style={{ alignItems: 'center', justifyContent: 'space-around' }}>
                            <Text category='h4' style={{ color: 'white', marginBottom: 30 }}>
                                aujourd'hui
                            </Text>
                            <View>
                                <Text category='h4' style={{ color: 'white' }}>
                                    {steps && steps.value}
                                </Text>
                                <Text style={{ alignSelf: "center" }} category='s2' appearance='hint'  >
                                    pas
                                </Text>
                            </View>
                        </View>
                        <Button style={styles.button} status='primary' onPress={handelClick} >
                            {steps && moment(ValidSteps.date).format('YYYY-MM-DD') !=moment(new Date).format('YYYY-MM-DD') ?steps.value +'  pas non validé': ValidSteps.StepsValid +'  pas validé'}
                        </Button>
                    </View>
                </View>
                <ModalWithBackdropShowcase dataModal={openModal} steps={steps.value} openCheck={(value) => setOpenModal(openModal.open=value)} />
            </ImageBackground>
     </ErrorBoundary>
    );
};

const styles = StyleSheet.create({
    button: {
        width: 190,
        backgroundColor: '#dc143c',
        borderColor: "red"
    },
    image: {
        flex: 1,
        resizeMode: "cover",
        paddingHorizontal: 30,
        paddingTop: 50
    },
    
});

export default Home;
