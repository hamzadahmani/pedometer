import React from 'react';
import { StyleSheet, View, Image, StatusBar } from 'react-native';
import { Button, Card, Layout, Text } from '@ui-kitten/components';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { useSelector } from 'react-redux';
import moment from 'moment'
import 'moment/locale/fr';
moment.locale('fr');

const History = (props) => {
    const steps = useSelector(state => state.homeReducer.Weekly)

    const CardStep = ({ data },key) => {
        return (
            <Card key={key} style={{marginVertical:20}}>
                <View style={{justifyContent: 'space-between', flexDirection: 'row' }}>
                    <View>
                        <Text category='h6' >{moment(data.date).format('LL') }</Text>
                    </View>
                    <View style={{ flexDirection:'row',alignItems:'center'}}>  
                        <Text category='h6'  >
                            {data.value}
                        </Text>
                        <Icon style={{paddingLeft:10}} name="shoe-prints" size={20} color="#100" />
                    </View>
                </View>
            </Card>)
    }

    return (
        <View  style={styles.container }>
            <Text style={styles.title} category='h5'>Vos pas hebdomadaires</Text>
            {steps.length != 0 && steps.map((el, index) => { return <CardStep data={el} key={index} /> })}
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        marginTop:40 
    },
    title: {
        textAlign:'center'
    },
  
    button: {
        alignSelf: 'center',
        width: 190,
        backgroundColor: '#dc143c',
        borderColor: "red"
    },

});

export default History;
