import React from 'react';
import { StyleSheet, View, Image } from 'react-native';
import { Button, Card, Modal, Text } from '@ui-kitten/components';

const ModalWithBackdropShowcase = (props) => {
 

    return (
        <View >
            <Modal
                visible={props.dataModal.open}
                backdropStyle={styles.backdrop}
                onBackdropPress={() => props.openCheck(false)}
            >
                <Card style={styles.container} disabled={true}>
                    <Image
                        style={styles.img}
                        source={require('../assets/run.jpg')} />
                    <Text style={{ textAlign: 'center',marginVertical:10 }}>{props.dataModal.message}</Text>
                    <Button style={styles.button} onPress={() => props.openCheck(false)}>
                        OK
                    </Button>
                </Card>
            </Modal>

        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        backgroundColor:'#F6F6F6',
        marginHorizontal: 10
    },
    backdrop: {
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
    },
    img: {
        height: 200,
        width: 200,
        alignSelf: 'center',
    },
    button: {
        alignSelf: 'center',
        width: 190,
        backgroundColor: '#dc143c',
        borderColor: "red"
    },

});

export default ModalWithBackdropShowcase;
