import { WEEKLY_STEPS, DAILY_STEPS, VALID_STEPS } from './type';
import { request, PERMISSIONS } from 'react-native-permissions';
import AsyncStorage from '@react-native-community/async-storage';
import moment from 'moment'
import GoogleFit, { Scopes } from 'react-native-google-fit'

//active app Permission to use ACTIVITY_RECOGNITION of user !required 
export const runPermission =  () => {
  
   return function (disaptch)  {
     request(PERMISSIONS.ANDROID.ACTIVITY_RECOGNITION).then(
         (PermActivity)=>{
            if (PermActivity == 'granted') {
                disaptch(auth())
            }
            else {
                disaptch(runPermission())  
            }
        })
    }
}

 
//get data saved into AsyncStorage 
export const getData = () => {
    return (disaptch) => {
        AsyncStorage.getItem('@userJackpot').then((value)=>{
            const jsonValue = value != null ? JSON.parse(value) : null
            if (jsonValue !== null) {
                // value previously stored
                  if (jsonValue.date) {
                    disaptch(setValidSteps({ date: jsonValue.date, valide: moment(jsonValue.date).format('L') == moment().format('L') ? true : false, jackpot:jsonValue.jackpot ,StepsValid: jsonValue.StepsValid }))
                }
            }
        })
      
    }
}

//save data into AsyncStorage after user confirm his progress   
export const setData =  (obj) => {

    return async(disaptch)=>{
        try {
            await AsyncStorage.setItem('@userJackpot', JSON.stringify(obj))
        } catch (e) {
            console.log('err', e);
        }
         disaptch(setValidSteps(obj))

    }
}

//start recording Activity and use APIs google fit 
const DailySteps = () => {
     return(disaptch)=>{
        GoogleFit.startRecording(callback => {
            disaptch(fetchData())
        }, ['step']);
    }
    
}

//after check permission enabled authorize user with only FITNESS_ACTIVITY scope we needed to active GoogleFit APIs and record steps
function auth() {
    const options = {
        scopes: [
            Scopes.FITNESS_ACTIVITY_READ,
            Scopes.FITNESS_ACTIVITY_WRITE,
        ],
    }
    return   (disaptch) => {
    GoogleFit.isEnabled(() => {
        GoogleFit.checkIsAuthorized().then(() => {
            if (!GoogleFit.isAuthorized) {
                GoogleFit.authorize(options)
                    .then(authResult => {
                        if (authResult.success) {
                            disaptch(DailySteps())  
                            console.log("AUTH_SUCCESS")
                        } else {
                            console.log("AUTH_DENIED", authResult.message)
                        }
                    })
                    .catch(() => {
                        console.log("AUTH_ERROR")
                    })
            }  
        })
    })
}
}



//fetch daily steps of user from GoogleFit api using merge_step_deltas source 
const fetchData =  () => {
     var dateDaily = new Date();
    dateDaily.setHours(0, 0, 0);
    const options = {
        startDate: dateDaily, // required
        endDate: new Date().toISOString(), // required
        bucketUnit: "DAY", // optional - default "DAY". Valid values: "NANOSECOND" | "MICROSECOND" | "MILLISECOND" | "SECOND" | "MINUTE" | "HOUR" | "DAY"
        bucketInterval: 1, // optional - default 1. 
    }
    return   (disaptch) => {
     GoogleFit.getDailyStepCountSamples(options).then((res)=>{
        console.log('resssdaily',res)
         res.map(el => {
            if (el.source === "com.google.android.gms:merge_step_deltas") { el.steps.length != 0 && disaptch(setDailySteps(el.steps[0])) }
            else{
                if (el.source === "com.google.android.gms:estimated_steps" && el.steps.length != 0 )  {   disaptch(setWeeklySteps(el.steps[0]))  }
             
            }
        })

    })
        GoogleFit.getWeeklySteps().then((res) =>{
            console.log('resss',res)
            res.map(el => {
                if (el.source === "com.google.android.gms:merge_step_deltas" && el.steps.length != 0 ) {   disaptch(setWeeklySteps(el.steps))  }
            })
            
        }).catch()
        disaptch(getData()) 
    }

}


export function setDailySteps(data) {
    return {
        type: DAILY_STEPS,
        payload: data,
    };
}

export function setWeeklySteps(data) {
     return {
        type: WEEKLY_STEPS,
        payload: data,
    };

};

export function setValidSteps(data) {
     return {
        type: VALID_STEPS,
        payload: data,
    };

};