import React from 'react';
import {ErrorScreen} from '../components/ErrorScreen';

/*
c un écrans de secours pour l'application
qui s'affiche lorsqu'une erreur JavaScript se produit au moment de l'exécution
pour evité le crash total et l'écran en rouge
*/
class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = {hasError: false};
  }

  static getDerivedStateFromError(error) {
    return {hasError: true};
  }

  componentDidCatch(error, errorInfo) {
    this.setState({hasError: true});
  }

  render() {
    if (this.state.hasError || this.props.throwTimeoutError) {
       return (
        <ErrorScreen
          refreshComponent={this.props.refreshComponent}
        />
      );
    }
    return this.props.children;
  }
}
export default ErrorBoundary;
