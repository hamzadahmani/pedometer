import {createStore, combineReducers, applyMiddleware} from "redux";
import thunk from 'redux-thunk';
import homeReducer from './reducers/homeReducer';

 export default createStore(
    combineReducers({
        homeReducer
    }),
    applyMiddleware(thunk)
);
